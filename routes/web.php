<?php

use Illuminate\Support\Facades\Route;

use App\Artikel;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/data-artikel/{name_artikel}',  array('as' => 'name_artikel', 'uses' => 'ArtikelsController@generateArtikel'));

Route::get('/data-artikel/{name_artikel}/{id}', array('as' => 'generateArtikel', 'uses' => 'ArtikelsController@generateArtikel'));

Route::get('/artikell/{artikel_id}/{user_id}',array('as' => 'generateArtikel', 'uses' => 'ArtikelsController@detailArtikel'));
Route::get('/artikell/{artikel_id}',array('as' => 'generateArtikel', 'uses' => 'ArtikelsController@detailArtikel'));

Route::get('/log', function () {
    return view('login');
});

Route::get('/log/{error}', function ($err) {
    return view('login', compact('err'));
});

Route::get('/log/{reg}/log', function ($reg) {
    return view('login', compact('reg'));
});

Route::get('/reg', function () {
    return view('register');
});

Route::get('/reg/{error}', function ($err) {
    return view('register', compact('err'));
});

Route::post('/newwrite', 'ArtikelsController@addArtikel');

Route::get('/newwrite/{is_admin}/{id}/{name}', function ($is_admin, $id, $name){
    return view('newartikel', compact('is_admin','id','name'));
});

Route::post('/reg', "UsersController@insertUser");

Route::post('/index', "UsersController@toIndex");

Route::get('/index/{isadmin}/{id}/{name}', function ($isadmin,$id,$name) {
    return view('index', compact('isadmin', 'id', 'name'));
});

Route::get('/claps/{id_artikel}/{id_user}',array('as' => 'addClaps', 'uses' => 'ArtikelsController@addClaps'));
Route::get('/removeClaps/{id_artikel}/{id_user}',array('as' => 'removeClaps', 'uses' => 'ArtikelsController@removeClaps'));

Route::post('/addComment', "ArtikelsController@addComment");
Route::post('/addSubComment',array('as' => 'addSubComment', 'uses' => 'ArtikelsController@addSubComment'));

Route::get('/addLike/{id_artikel}/{id_user}/{comment_id}/{dislike_id}/{type}',array('as' => 'addLike', 'uses' => 'ArtikelsController@addLike'));
Route::get('/removeLike/{id_artikel}/{id_user}/{like_id}',array('as' => 'removeLike', 'uses' => 'ArtikelsController@removeLike'));
Route::get('/addDislike/{id_artikel}/{id_user}/{comment_id}/{like_id}/{type}',array('as' => 'addLike', 'uses' => 'ArtikelsController@addDislike'));
Route::get('/removeDislike/{id_artikel}/{id_user}/{dislike_id}',array('as' => 'removeLike', 'uses' => 'ArtikelsController@removeDislike'));

Route::get('/myartikel/{user_id}', array('as' => 'generateData', 'uses' => 'MyArtikelController@generateData'));
Route::get('/delete_artikel/{artikel_id}/{user_id}', array('as' => 'deleteData', 'uses' => 'MyArtikelController@deleteData'));
Route::get('/update_artikel/{artikel_id}/{user_id}', array('as' => 'updateData', 'uses' => 'MyArtikelController@updateArtikelData'));
Route::post('/ubah_data_artikel', array('as' => 'updateData', 'uses' => 'MyArtikelController@ubahDataArtikel'));

Route::get('/statistic/{user_id}', array('as' => 'data', 'uses' => 'StatisticsController@index'));
