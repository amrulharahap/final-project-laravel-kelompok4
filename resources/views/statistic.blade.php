@extends('layouts.master')

@push('style')
    <link rel="stylesheet" href="{{asset('artikel/myartikel.css')}}">
@endpush

@section('main')
    <div class="statictic_user">
        <h1 class="mt-2">Statistik User</h1><br>
        <table border="1px" class="myartikel-table" cellspacing="0" cellpadding="0">
            <tr>
                <th>NO</th>
                <th>Name</th>
                <th>Email</th>
                <th>Created At</th>
                <th>Is Admin</th>
            </tr>
            @php
                $i = 1
            @endphp
            @foreach($dataUsers as $user)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$user['name']}}</td>
                    <td>{{$user['email']}}</td>
                    <td>{{$user['created_at']}}</td>
                    <td>{{$user['is_admin']}}</td>
            @endforeach
        </table>
    </div>

    <div class="statictic_Artikel">
        <h1 class="mt-4">Statistik Artikel</h1><br>
        <table border="1px" class="myartikel-table" cellspacing="0" cellpadding="0">
            <tr>
                <th>NO</th>
                <th>Type</th>
                <th>Title</th>
                <th>Writer</th>
                <th>Created At</th>
                <th>Updated At</th>
            </tr>
            @php
                $i = 1
            @endphp
            @foreach($dataArtikels as $artikel)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$artikel['type']}}</td>
                    <td>{{$artikel['title']}}</td>
                    <td>{{$artikel['writer']}}</td>
                    <td>{{$artikel['created_at']}}</td>
                    <td>{{$artikel['updated_at']}}</td>
            @endforeach
        </table>
    </div>

    <div class="statictic_Komentar">
        <h1 class="mt-4">Statistik Komentar</h1><br>
        <table border="1px" class="myartikel-table" cellspacing="0" cellpadding="0">
            <tr>
                <th>NO</th>
                <th>User</th>
                <th>Create_At</th>
            </tr>
            @php
                $i = 1
            @endphp
            @foreach($dataComments as $comment)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$comment['username']}}</td>
                    <td>{{$comment['created_at']}}</td>
            @endforeach
        </table>
    </div>


@endsection
