@extends('layouts.master')

@push('style')
    <link rel="stylesheet" href="{{asset('artikel/myartikel.css')}}">
@endpush

@section('main')

    @if(isset($id))
        <div style="text-align: right;margin-top: 30px;margin-right: 30px" class="write">
            <a href="{{url('newwrite/'.$isadmin.'/'.$id.'/'.$name)}}">Tulis Artikel <i class="fas fa-pen ml-2"></i></a>
        </div>
    @endif

    <h1 class="mt-2">My Artikel</h1><br>
    <table border="1px" class="myartikel-table" cellspacing="0" cellpadding="0">
        <tr>
            <th>NO</th>
            <th>Title</th>
            <th>Create_at</th>
            <th>Action</th>
        </tr>
        @php
            $i = 1
        @endphp
        @foreach($myartikels as $myartikel)
            <tr>
                <td>{{$i++}}</td>
                <td><a href="{{url('artikell/'.$myartikel['id'].'/'.$id)}}">{{$myartikel['title']}}</a></td>
                <td>{{$myartikel['created_at']}}</td>
                <td>
                    <a href="/update_artikel/{{$myartikel['id']}}/{{$id}}">Update</a> |

                    <a href="/delete_artikel/{{$myartikel['id']}}/{{$id}}" onclick="return confirm('yakin?');">Delete</a>
                </td>
            </tr>
        @endforeach
    </table>
@endsection
