<html>
<head>
    <link rel="stylesheet" href="{{asset('login/style.css')}}">
</head>

<body>
<h1>Registrasi</h1>
<form action="/reg" method="POST">
    @csrf
    <label for="name">Name: </label>
    <input type="text" placeholder="Name" name="name" id="name" required autocomplete="off" autofocus>
    <label for="name">Email: </label>
    <input type="email" placeholder="Email" name="email" id="email" required autocomplete="off" autofocus>
    <label for="password">Password : </label>
    <input type="password" placeholder="Password" name="password" id="password" required autocomplete="off">
    <label for="password2">Konfirmasi Password : </label>
    <input type="password" placeholder="Konfirmasi Password" name="password2" id="password2" required autocomplete="off">
    @if(isset($err))
        <p style="color:red;font-style: italic">Konfirmasi Password Tidak Sama</p>
    @endif
    <input type="submit" name="submit" value="Registrasi" onsubmit="hasBennSubmit">
</form>
</body>
</html>
