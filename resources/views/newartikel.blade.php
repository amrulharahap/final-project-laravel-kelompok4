<html>
<head>
    <link rel="stylesheet" href="{{asset('login/newartikel.css')}}">

{{--    <script src="ckeditor/ckeditor.js"></script>--}}
    <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

    <link rel="stylesheet" href="{{asset('home/css/bootstrap.min.css')}}"/>
    <style>
        .container {
            width: 1000px;
            margin :30px auto;
        }

        form * {
            margin-bottom: 10px;
        }

        .container h1 {
            display: inline;
            border-bottom: 2px solid #33333333;
            margin-bottom: 100px;
        }
    </style>
</head>

<body>
    <div class="container">
        <h1>New Artikel</h1><br><br>
        <form action="/newwrite" method="post">
            @csrf
            <input type="hidden" name="id" value="{{$id}}">
            <input type="hidden" name="is_admin" value="{{$is_admin}}">
            <input type="hidden" name="name" value="{{$name}}">
            <select class="form-control" onchange="con()" id="topik" name="topik">
                <option value="0">--Pilih Topik--</option>
                <option value="1">Bahasa Pemograman</option>
                <option value="2">Komputer</option>
                <option value="3">News</option>
            </select>
            <select onchange="cek()" class="form-control" id="subtopik" name="subtopik" disabled>
                <option value="0">--Pilih Sub Topik--</option>
                <option value="1">laravel</option>
                <option value="2">PHP</option>
                <option value="3">Javascript</option>
                <option value="4">Java</option>
                <option value="5">Kotlin</option>
                <option value="6">Golang</option>
                <option value="7">Swift</option>
                <option value="8">Python</option>
            </select>
            <input onchange="cek()" placeholder="Judul" class="form-control" type = "text" id="title" name="title" autocomplete="off">
            <textarea onchange="cek()" id="editor" class="form-control" name="text-editor-artikel"></textarea>

            <input class="btn btn-primary" id="kirim" type="submit" name="kirim" value="Kirim" onsubmit="Login">
        </form>

            <script>
                CKEDITOR.replace( 'text-editor-artikel' );

                var btnSend = document.getElementById("kirim");
                btnSend.disabled = true;

                function con() {
                    cek();
                    var topik = document.getElementById("topik").value;
                    var subtopik = document.getElementById("subtopik");
                    console.log(topik);
                    if(topik== 0 || topik == 2 || topik == 3) {
                        subtopik.disabled = true;
                    }else {
                        subtopik.disabled = false;
                    }
                }

                cek();

                function cek() {
                    var topik = document.getElementById("topik").value;
                    var subtopik = document.getElementById("subtopik");
                    var title = document.getElementById("title");
                    var editor = document.getElementById("editor");
                    var btnSend = document.getElementById("kirim");

                    if (topik == 0 || title.value.trim() == "") {
                        btnSend.disabled = true;
                    } else if (topik == 1 && subtopik.value == '-1') {
                        btnSend.disabled = true;
                    } else {
                        btnSend.disabled = false;
                    }
                }

            </script>
    </div>
</body>
</html>
