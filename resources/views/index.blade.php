@extends("layouts.master")

@section('main')
    <section style= "margin-top : -80px" class="playlist-section spad">

        @if(isset($id))
            <div style="text-align: right;padding-bottom: 30px" class="write">
                <a href="{{url('newwrite/'.$isadmin.'/'.$id.'/'.$name)}}">Tulis Artikel <i class="fas fa-pen ml-2"></i></a>
            </div>
        @endif

        <div class="container-fluid">
            <div class="section-title">
                <h2>List</h2>
            </div>
            <div class="container">
                <ul class="playlist-filter controls">
                    <li class="control" data-filter=".genres">Bahasa pemograman</li>
                    <li class="control" data-filter=".artists">Komputer</li>
                    <li class="control" data-filter=".movies">News</li>
                    <li class="control" data-filter="all">All Item</li>
                </ul>
            </div>
            <div class="clearfix"></div>
            <div style= "margin-top : -50px;" class="row playlist-area">
                <div class="mix col-lg-3 col-md-4 col-sm-6 genres">
                    @if(isset($id))
                        <a style="text-decoration: none" href = "{{url('data-artikel/1/'.$id)}}">
                            <div class="playlist-item">
                                <img src="{{asset('home/img/playlist/1.jpg')}}" alt="">
                                <h5>Laravel</h5>
                            </div>
                        </a>
                    @else
                        <a style="text-decoration: none" href = "{{url('data-artikel/1')}}">
                            <div class="playlist-item">
                                <img src="{{asset('home/img/playlist/1.jpg')}}" alt="">
                                <h5>Laravel</h5>
                            </div>
                        </a>
                    @endif
                </div>

                <div class="mix col-lg-3 col-md-4 col-sm-6 movies">
                    @if(isset($id))
                        <a style="text-decoration: none" href = "{{url('data-artikel/1/'.$id)}}">
                            <div class="playlist-item">
                                <img src="{{asset('home/img/playlist/2.jpg')}}" alt="">
                                <h5>Tentang TI</h5>
                            </div>
                        </a>
                    @else
                        <a style="text-decoration: none" href = "{{url('data-artikel/1')}}">
                            <div class="playlist-item">
                                <img src="{{asset('home/img/playlist/2.jpg')}}" alt="">
                                <h5>Tentang TI</h5>
                            </div>
                        </a>
                    @endif
                </div>

                <div class="mix col-lg-3 col-md-4 col-sm-6 movies">
                    @if(isset($id))
                        <a style="text-decoration: none" href = "{{url('data-artikel/1/'.$id)}}">
                            <div class="playlist-item">
                                <img src="{{asset('home/img/playlist/6.jpg')}}" alt="">
                                <h5>Tentang SI</h5>
                            </div>
                        </a>
                    @else
                        <a style="text-decoration: none" href = "{{url('data-artikel/1')}}">
                            <div class="playlist-item">
                                <img src="{{asset('home/img/playlist/6.jpg')}}" alt="">
                                <h5>Tentang SI</h5>
                            </div>
                        </a>
                    @endif
                </div>

                <div class="mix col-lg-3 col-md-4 col-sm-6 genres">
                    @if(isset($id))
                        <a style="text-decoration: none" href = "{{url('data-artikel/1/'.$id)}}">
                            <div class="playlist-item">
                                <img src="{{asset('home/img/playlist/4.jpg')}}" alt="">
                                <h5>Python</h5>
                            </div>
                        </a>
                    @else
                        <a style="text-decoration: none" href = "{{url('data-artikel/1')}}">
                            <div class="playlist-item">
                                <img src="{{asset('home/img/playlist/4.jpg')}}" alt="">
                                <h5>Python</h5>
                            </div>
                        </a>
                    @endif
                </div>

                <div class="mix col-lg-3 col-md-4 col-sm-6 artists">
                    @if(isset($id))
                        <a style="text-decoration: none" href = "{{url('data-artikel/1/'.$id)}}">
                            <div class="playlist-item">
                                <img src="{{asset('home/img/playlist/7.jpg')}}" alt="">
                                <h5>Tentang Komputer</h5>
                            </div>
                        </a>
                    @else
                        <a style="text-decoration: none" href = "{{url('data-artikel/1')}}">
                            <div class="playlist-item">
                                <img src="{{asset('home/img/playlist/7.jpg')}}" alt="">
                                <h5>Tentang Komputer</h5>
                            </div>
                        </a>
                    @endif
                </div>

                <div class="mix col-lg-3 col-md-4 col-sm-6 genres">
                    @if(isset($id))
                        <a style="text-decoration: none" href = "{{url('data-artikel/1/'.$id)}}">
                            <div class="playlist-item">
                                <img src="{{asset('home/img/playlist/8.jpg')}}" alt="">
                                <h5>Javascript</h5>
                            </div>
                        </a>
                    @else
                        <a style="text-decoration: none" href = "{{url('data-artikel/1')}}">
                            <div class="playlist-item">
                                <img src="{{asset('home/img/playlist/8.jpg')}}" alt="">
                                <h5>Javacript</h5>
                            </div>
                        </a>
                    @endif
                </div>

                <div class="mix col-lg-3 col-md-4 col-sm-6 genres">
                    @if(isset($id))
                        <a style="text-decoration: none" href = "{{url('data-artikel/1/'.$id)}}">
                            <div class="playlist-item">
                                <img src="{{asset('home/img/playlist/10.jpg')}}" alt="">
                                <h5>Bahasa C</h5>
                            </div>
                        </a>
                    @else
                        <a style="text-decoration: none" href = "{{url('data-artikel/1')}}">
                            <div class="playlist-item">
                                <img src="{{asset('home/img/playlist/10.jpg')}}" alt="">
                                <h5>Bahasa C</h5>
                            </div>
                        </a>
                    @endif
                </div>

            </div>
        </div>
    </section>
@endsection

@push('style')
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i&display=swap" rel="stylesheet">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{asset('home/css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('home/css/font-awesome.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('home/css/owl.carousel.min.css')}}"/>
        <link rel="stylesheet" href="{{asset('home/css/slicknav.min.css')}}"/>
    <!-- Main Stylesheets -->
    <link rel="stylesheet" href="{{asset('home/css/style.css')}}"/>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
@endpush

@push('script')
    <script src="{{asset('home/js/mixitup.min.js')}}"></script>
    <script src="{{asset('home/js/main.js')}}"></script>
@endpush
