@extends('layouts.master')

@section('main')
    <div class ='search'>
        <form method="" action="GET">
            <input  class="form-control" placeholder="search" name="search" id="search" type="text">
        </form>
    </div>

    <div class="row list-artikel">
        <div class="col-md-9">
            <div class="content-list-artikel">
                <h1 class="mt-1">{{$nameArtikel}}</h1>

                @foreach($data as $dt)
                    <article>
                        <div class="media">
                            @if(isset($id))
                                <a href="{{url('artikell/'.$dt['id'].'/'.$id)}}" class="media-left mr-3" >
                            @else
                                <a href="{{url('artikell/'.$dt['id'])}}" class="media-left mr-3" >
                            @endif
                                <img class="img-fluid" src="{{asset('artikel/img/1.jpg')}}" />
                            @if(isset($id)) </a>
                            @else </a>
                            @endif
                            <div class="media-body media-justyfied">
                                @if(isset($id))
                                    <a href="{{url('artikell/'.$dt['id'].'/'.$id)}}">
                                @else
                                    <a href="{{url('artikell/'.$dt['id'])}}">
                                @endif
                                    <h1 class="mt-0 media-heading">{{$dt['title']}}</h1>
                                @if(isset($id)) </a>
                                @else </a>
                                @endif
                                <div id="content_text_artikel" class="content_artikel">{!!$dt['content']!!}</div>
                                <p class="writenby">{{$dt['writer'] ?? ''}} <span class="date"> {{$dt['created_at']}}</span></p>
                            </div>
                        </div>
                    </article>
                @endforeach

            </div>
        </div>

        <aside class="col-md-3 col-sm-12">
            @include('layouts.top')
        </aside>
    </div>

@endsection

@push('style')
    <link rel="stylesheet" href="{{asset('artikel/style.css')}}">
@endpush

@push('script')
    <script>
        var ctn = document.querySelectorAll('.content_artikel');
        ctn.forEach(function(userItem) {
            if(userItem.innerHTML.length > 146)
                userItem.innerHTML = userItem.innerHTML.substr(0,150) + ".."
        })
    </script>
@endpush
