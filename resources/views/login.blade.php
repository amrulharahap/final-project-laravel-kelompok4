<html>
<head>
    <link rel="stylesheet" href="{{asset('login/style.css')}}">
</head>

<body>
@if(isset($reg))
    <h3 style="opacity: 0.6;margin-left: 30px">Data berhasil Berhasil Diinput</h3>
@endif
<h1>Login</h1>
<form action="/index" method="POST">
    @csrf
    <label for="email">Username : </label>
    <input type="text" placeholder="Email" name="email" id="email" required autocomplete="off" autofocus>
    <label for="password">Password : </label>
    <input type="password" placeholder="Password" name="password" id="password" required autocomplete="off">
    @if(isset($err))
        <p style="color:red;font-style: italic">Username atau password salah</p>
    @endif
{{--    <input style="display: inline" type="checkbox" name="rememberme" id="rememberme" value="1">--}}
{{--    <label style="display: inline" for="rememberme">Remember Me</label>--}}
    <input type="submit" name="login" value="Login" onsubmit="Login">

    <a id="signup" href = "{{url('reg')}}">Sign Up</a>
</form>
</body>
</html>
