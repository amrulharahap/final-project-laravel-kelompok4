
<div class = "komentar">
    <hr class="line-koment">
    <h5 class="sumKomentar">Komentar - <span class="bilkoment">{{$SUMCOMMENT}}</span></h5>
    <div class="form-komentar">
        @if(isset($id))<form action="/addComment" method="post">
        @else
        <form action="/log" method="get">
        @endif
            @csrf
            <input type="hidden" name="user_id" value="{{$id ?? ''}}">
            <input type="hidden" name="artikel_id" value="{{$data['id'] ?? ''}}">
            <textarea class="form-control" id="text-komen" name="text-komen"></textarea>
            <input class="btn btn-success" type="submit" name="komen" id="btn-koment" value="Kirim">

        @if(isset($id))
        </form>
        @else
        </form>
        @endif
    </div>

    <div class="media-list">
        @foreach($dataComment as $comment)
            <div class="media">
                <a href="#" class="mr-3 mt-3 media-left">
                    <img src="{{asset('artikel/img/1.jpg')}}" />
                </a>
                <div class="media-body">
                    <div class="wrap-body">
                        <h1 class="media-heading">{{$comment['username']}}</h1>
                        {{$comment['comment_text']}}

                        <div class="fitur-chat">
                            <div class="comment-date">{{$comment['created_at']}}</div>
                            <div class="btn-aksi">
                                @if(isset($id))
                                    <a class="btn btn-success btn-circle text-uppercase"
                                        href="@if($comment['liked']) {{url('/removeLike/'.$data['id'].'/'.$id.'/'.$comment['like_id'])}}
                                        @else {{url('/addLike/'.$data['id'].'/'.$id.'/'.$comment['id'].'/'.$comment['dislike_id'].'/comment')}} @endif" id="like">
                                        <span class="glyphicon glyphicon-share-alt"></span>
                                        @if(!$comment['liked']) <i class=" btn-like far fa-thumbs-up"></i>
                                        @else <i class="btn-like fas fa-thumbs-up"></i>@endif
                                        @if($comment['sumlikes']) {{$comment['sumlikes']}} @endif
                                    </a>

                                    <a class="btn btn-secondary btn-circle text-uppercase"
                                       href="@if($comment['disliked']) {{url('/removeDislike/'.$data['id'].'/'.$id.'/'.$comment['dislike_id'])}}
                                       @else {{url('/addDislike/'.$data['id'].'/'.$id.'/'.$comment['id'].'/'.$comment['like_id'].'/comment')}} @endif" id="dislike">
                                        <span class="glyphicon glyphicon-share-alt"></span>
                                        @if(!$comment['disliked'])<i class="btn-like far fa-thumbs-down"></i>
                                        @else <i class="btn-like fas fa-thumbs-down"></i>@endif
                                        @if($comment['sumdislikes']) {{$comment['sumdislikes']}} @endif
                                    </a>

                                    <a class="btn btn-warning btn-circle text-uppercase" data-toggle="collapse" href="#collapsekomen{{$comment['id']}}">
                                        <span class="glyphicon glyphicon-comment"></span>  {{count($comment['subComment'])}} comments
                                        <i class="fas fa-reply"></i>
                                    </a>

                                @else
                                    <a class="btn btn-success btn-circle text-uppercase" id="like">
                                        <span class="glyphicon glyphicon-share-alt"></span>
                                        <i class="btn-like far fa-thumbs-up"></i>
                                        @if($comment['sumlikes']) {{$comment['sumlikes']}} @endif
                                    </a>
                                    <a class="btn btn-secondary btn-circle text-uppercase" id="dislike">
                                        <span class="glyphicon glyphicon-share-alt"></span>
                                        <i class="btn-like far fa-thumbs-down"></i>
                                        @if($comment['sumdislikes']) {{$comment['sumdislikes']}} @endif
                                    </a>
                                    <a class="btn btn-warning btn-circle text-uppercase" data-toggle="collapse" href="#collapsekomen{{$comment['id']}}">
                                        <span class="glyphicon glyphicon-comment"></span>  {{count($comment['subComment'])}} comments
                                        <i class="fas fa-reply"></i>
                                    </a>
                                @endif

                            </div>
                        </div>
                    </div>

                    <div class="sub-koment collapse" id="collapsekomen{{$comment['id']}}">
                        @if(isset($id))
                        <form action= "/addSubComment" method="post">
                        @else <form action= "/log" method="get">
                        @endif
                            @csrf
                            <input type="hidden" name="user_id" value="{{$id ?? ''}}">
                            <input type="hidden" name="artikel_id" value="{{$data['id'] ?? ''}}">
                            <input type="hidden" name="comment_id" value="{{$comment['id'] ?? ''}}">
                            <textarea class="form-control" id="sub_comment_text" name="sub_comment_text"></textarea>
                            <input style="margin-bottom: 20px;margin-top: 10px" class="btn btn-success" type="submit" name="subkomen" id="btn-koment" value="Kirim">
                        @if(isset($id)) </form>
                        @else </form> @endif

                        @foreach($comment['subComment'] as $subComment)
                            <div class="media">
                                <a href="#" class="mr-3 mt-3 media-left">
                                    <img src="{{asset('artikel/img/2.jpg')}}" />
                                </a>
                                <div class="media-body">
                                    <div class="wrap-body">
                                        <h1 class="media-heading">{{$subComment['username']}}</h1>
                                        {{$subComment['comment_text']}}

                                        <div class="fitur-chat">
                                            <div class="comment-date">{{$subComment['created_at']}}</div>
                                            <div class="btn-aksi">
                                                @if(isset($id))
                                                    <a class="btn btn-success btn-circle text-uppercase"
                                                       href="@if($subComment['liked']) {{url('/removeLike/'.$data['id'].'/'.$id.'/'.$subComment['like_id'])}}
                                                       @else {{url('/addLike/'.$data['id'].'/'.$id.'/'.$subComment['id'].'/'.$subComment['dislike_id'].'/subcomment')}} @endif" id="like">
                                                        <span class="glyphicon glyphicon-share-alt"></span>
                                                        @if(!$subComment['liked']) <i class=" btn-like far fa-thumbs-up"></i>
                                                        @else <i class="btn-like fas fa-thumbs-up"></i>@endif
                                                        @if($subComment['sumlikes']) {{$subComment['sumlikes']}} @endif
                                                    </a>

                                                    <a class="btn btn-secondary btn-circle text-uppercase"
                                                       href="@if($subComment['disliked']) {{url('/removeDislike/'.$data['id'].'/'.$id.'/'.$subComment['dislike_id'])}}
                                                       @else {{url('/addDislike/'.$data['id'].'/'.$id.'/'.$subComment['id'].'/'.$subComment['like_id'].'/subcomment')}} @endif" id="dislike">
                                                        <span class="glyphicon glyphicon-share-alt"></span>
                                                        @if(!$subComment['disliked'])<i class="btn-like far fa-thumbs-down"></i>
                                                        @else <i class="btn-like fas fa-thumbs-down"></i>@endif
                                                        @if($subComment['sumdislikes']) {{$subComment['sumdislikes']}} @endif
                                                    </a>

                                                @else
                                                    <a class="btn btn-success btn-circle text-uppercase" id="like">
                                                        <span class="glyphicon glyphicon-share-alt"></span>
                                                        <i class="btn-like far fa-thumbs-up"></i>
                                                        @if($subComment['sumlikes']) {{$comment['sumlikes']}} @endif
                                                    </a>
                                                    <a class="btn btn-secondary btn-circle text-uppercase" id="dislike">
                                                        <span class="glyphicon glyphicon-share-alt"></span>
                                                        <i class="btn-like far fa-thumbs-down"></i>
                                                        @if($subComment['sumdislikes']) {{$comment['sumdislikes']}} @endif
                                                    </a>
                                                @endif

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
