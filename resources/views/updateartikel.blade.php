<html>
<head>
    <link rel="stylesheet" href="{{asset('login/newartikel.css')}}">

{{--    <script src="ckeditor/ckeditor.js"></script>--}}
    <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

    <link rel="stylesheet" href="{{asset('home/css/bootstrap.min.css')}}"/>
    <style>
        .container {
            width: 1000px;
            margin :30px auto;
        }

        form * {
            margin-bottom: 10px;
        }

        .container h1 {
            display: inline;
            border-bottom: 2px solid #33333333;
            margin-bottom: 100px;
        }
    </style>
</head>

<body>
    <div class="container">
        <h1>Update Artikel</h1><br><br>
        <form action="/ubah_data_artikel" method="post">
            @csrf
            <input type="hidden" name="artikel_id" value="{{$data_update_artikel['id']}}">
            <input type="hidden" name="user_id" value="{{$id}}">
            <input type="text" class="form-control" value="{{$data_update_artikel['type']}}" id="topik" name="topik">
            <input type="text"  class="form-control" value="{{$data_update_artikel['sub_type']}}" id="subtopik" name="subtopik">
            <input value="{{$data_update_artikel['title']}}" class="form-control" type = "text" id="title" name="title" autocomplete="off">
            <textarea id="editor" class="form-control" name="text-editor-artikel">{!!$data_update_artikel['content']!!}
            </textarea>
            <input class="btn btn-primary" id="kirim" type="submit" name="kirim" value="Kirim" onsubmit="Login">
        </form>

            <script>
                CKEDITOR.replace( 'text-editor-artikel' );
                var topik = document.getElementById("topik");
                var subtopik = document.getElementById("subtopik");
                var title = document.getElementById("title");
                topik.disabled = true;
                subtopik.disabled = true;
                title.disabled = true;

            </script>
    </div>
</body>
</html>
