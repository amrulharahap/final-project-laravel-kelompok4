<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Medium Mini</title>
    <link href="{{asset('sbadmin/css/styles.css')}}" rel="stylesheet" />

    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>

    @stack('style')

</head>
<body class="sb-nav-fixed">
<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
    <a class="navbar-brand" href="index.html">Start Bootstrap</a><button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button
    ><!-- Navbar Search-->

    <!-- Navbar-->
    <ul class="navbar-nav ml-auto mr-auto ml-md-0">

    </ul>

    @if(isset($name))
        <div style="color:white" class="username">
            {{$name}} ( <a href="{{url('/')}}" class="logout">Log Out</a> )
        </div>
    @else
        <div style="color:white" class="username">
            Anda Belum Login ( <a href="{{url('log')}}" class="logout">Log In</a> )
        </div>
    @endif

</nav>
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
            <div class="sb-sidenav-menu">
                <div class="nav">
                    <div class="sb-sidenav-menu-heading">Core</div>

                    @if(!isset($id))
                        <a class="nav-link" href="{{url('/')}}">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                            Dashboard</a>
                    @else
                        <a class="nav-link" href="{{url('/index/'.$isadmin.'/'.$id.'/'.$name)}}">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                            Dashboard</a>
                    @endif

                    <a style="color:white" class="nav-link" href="{{url('/reg')}}">
                        <div class="sb-nav-link-icon"><i style="color:white;font-size: 18px"  class="far fa-registered"></i></div>
                        Register</a>

                    @if(isset($id))
                        <a style="color:white" class="nav-link" href="{{url('/myartikel/'.$id)}}">
                            <div class="sb-nav-link-icon"><i style="color:white;font-size: 18px" class="fas fa-list"></i></div>
                            My Artikel
                        </a>
                    @endif

                    @if(isset($id) && $isadmin == 1)
                        <a style="color:white" class="nav-link" href="{{url('/statistic/'.$id)}}">
                            <div class="sb-nav-link-icon"><i style="color:white;font-size: 18px" class="fas fa-chart-bar"></i></div>
                            Statistik
                        </a>
                    @endif


                </div>
            </div>
            <div class="sb-sidenav-footer">
                <div class="small">Logged in as:</div>
                @if(isset($name))
                    @if($isadmin == 1)
                        Admin
                    @else
                        Writer
                    @endif
                @else
                    Guest
                @endif
            </div>
        </nav>
    </div>
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                @yield('main')
            </div>
        </main>

        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="{{asset('sbadmin/js/scripts.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
<script src="assets/demo/chart-area-demo.js"></script>
<script src="assets/demo/chart-bar-demo.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="assets/demo/datatables-demo.js"></script>


@stack('script')

</body>
</html>
