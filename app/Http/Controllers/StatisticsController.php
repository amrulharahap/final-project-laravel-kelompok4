<?php

namespace App\Http\Controllers;

use App\Artikel;
use App\Comment;
use App\User;
use Illuminate\Http\Request;

class StatisticsController extends Controller
{
    public function index($user_id) {
        $users = User::all();
        $dataUsers = [];
        foreach ($users as $user) {
            if($user['is_admin'] == 1) $user['is_admin'] = "YES";
            else $user['is_admin'] = "NO";
            $dataUsers[] = $user;
        }

        $artikels = Artikel::all();
        $dataArtikels = [];
        foreach ($artikels as $artikel) {
            if($artikel['type'] == 1) $artikel['type']  = "Laravel";
            else if($artikel['type'] == 2) $artikel['type']  = "Tentang TI";
            else if($artikel['type'] == 3) $artikel['type']  = "Sistem Operasi";

            $users = User::where('id', $artikel['id_user'])->get();
            foreach ($users as $user) {
                $artikel['writer'] = $user['name'];
            }
            $dataArtikels[] = $artikel;
        }

        $comments = Comment::all();
        $dataComments = [];
        foreach ($comments as $comment) {
            $users = User::where('id', $comment['user_id'])->get();
            foreach ($users as $user) {
                $comment['username'] = $user['name'];
            }
            $dataComments[] = $comment;
        }

        $users = User::where('id', $user_id)->get();
        foreach ($users as $user) {
            $name = $user['name'];
            $isadmin = $user['is_admin'];
        }
        $id = $user_id;

        return view("statistic", compact('dataUsers', 'dataArtikels', 'dataComments','id', 'name', 'isadmin'));
    }
}
