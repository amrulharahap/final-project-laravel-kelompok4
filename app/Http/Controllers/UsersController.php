<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use function Sodium\compare;

class UsersController extends Controller
{
    public function insertUser(Request $request) {
        $name = $request['name'];
        $email = $request['email'];
        $password = $request['password'];
        $password2 = $request['password2'];

        if($password !== $password2) {
            return redirect("reg/err");
            exit();
        }

        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password = $password;
        $user->is_admin = 0;

        $user->save();

        return redirect("/log/reg/log");
    }

    public function toIndex(Request $request) {

        $email= $request['email'];
        $pass = $request['password'];

        $user = User::where('email', $email)->where('password',$pass)->get() ;

        if(count($user) ==  1)
        {
            $id = 0;
            $name = "";
            $is_admin = "";
            foreach ($user as $us)
            {
                $name =  $us->name;
                $is_admin = $us->is_admin;
                $id = $us->id;
            }
            return redirect('index/'.$is_admin.'/'.$id.'/'.$name);
        }
        else
            return redirect('log/error');
    }

}
