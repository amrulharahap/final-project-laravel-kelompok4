<?php

namespace App\Http\Controllers;

use App\Artikel;
use App\Clap;
use App\Comment;
use App\Subcomment;
use App\User;
use App\Like;
use App\Dislike;
use Illuminate\Http\Request;

class MyArtikelController extends Controller
{
    public function generateData($user_id) {
        $artikels = Artikel::latest()->where('id_user', $user_id)->get();
        $myartikels = [];
        foreach ($artikels as $artikel) {
            $myartikels[] = $artikel;
        }

        $id = $user_id;
        $users = User::where('id', $id) -> get();
        foreach ($users as $user) {
            $name = $user['name'];
            $isadmin = $user['is_admin'];
        }
        return view('myartikel', compact('myartikels', 'id', 'name','isadmin'));
    }

    public function deleteData($artikel_id,$user_id)
    {
        $artikel = Artikel::where('id', $artikel_id);
        $claps = Clap::where('id', $artikel_id);
        $like = Like::where('id', $artikel_id);
        $dislike = Dislike::where('id', $artikel_id);
        $comment= Comment::where('id', $artikel_id);
        $subcomment= Subcomment::where('id', $artikel_id);

        if($artikel) $artikel->delete();
        if($claps) $claps->delete();
        if($like) $like->delete();
        if($dislike) $dislike->delete();
        if($comment) $comment->delete();
        if($subcomment) $subcomment->delete();

        return redirect('myartikel/'.$user_id);
    }

    public function updateArtikelData($artikel_id, $user_id) {
//        $topik = $request['topik'];
//        $subtopik = $request['subtopik'];
//        $title = $request['title'];
//
//        $name = $request['name'];
//        $is_admin = $request['is_admin'];
//        $id = $request['id'];
        $artikels = Artikel::where("id", $artikel_id)->get();
        $data_update_artikel = [];
        foreach ($artikels as $artikel) {
            if($artikel["type"] == "1")
                $artikel['type'] = "Bahasa Pemograman";
            else if($artikel['type'] == "2")
                $artikel['type'] = "Tentang TI";
            else
                $artikel['type'] = "Komputer";

            if($artikel['sub_type'] == "1")
                $artikel['sub_type'] = "Laravel";
            else if($artikel["sub_type"] == "2")
                $artikel['sub_type'] = "PHP";
            else
                $artikel['sub_type'] = "Other";

            $data_update_artikel = $artikel;
        }
        $id = $user_id;
        return view('updateartikel', compact('data_update_artikel', 'id'));
    }

    public function ubahDataartikel(Request $request){
        $content = $request['text-editor-artikel'];
        $user_id = $request['user_id'];
        $artikel_id = $request['artikel_id'];

        $artikels = Artikel::find($artikel_id);
        $artikels->content = $content;
        $artikels->save();
        return redirect('/myartikel/'.$user_id);
    }
}
