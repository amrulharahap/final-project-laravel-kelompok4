<?php

namespace App\Http\Controllers;

use App\Artikel;
use App\Clap;
use App\Comment;
use App\Like;
use App\Dislike;
use App\Subcomment;
use App\User;
use Illuminate\Http\Request;
use function Sodium\compare;

class ArtikelsController extends Controller
{
    public function addArtikel(Request $request)
    {
        $id_user = $request['id'];
        $name = $request['name'];
        $isAdmin = $request['is_admin'];
        $topic = $request['topik'];
        $subtopik  = $request['subtopik'];
        $title = $request['title'];

        $content = $request['text-editor-artikel'];

        if(!$subtopik) $subtopik = "-1";

        $artikels = new Artikel();

        $artikels->title = $title;
        $artikels->type = $topic;
        $artikels->sub_type = $subtopik;
        $artikels->content = $content;
        $artikels->id_user = $id_user;
        $artikels->claps = 0;

        $artikels->save();
        return redirect("index/".$isAdmin.'/'.$id_user.'/'.$name);
    }

    public function generateArtikel($name_artikel, $id=-1) {
        if($name_artikel == 1)
            $nameArtikel = "Laravel";
        else if($name_artikel == 2)
            $nameArtikel = "TentangTI";
        else
            $nameArtikel = "Other";

        $artikels = Artikel::orderBy('id', 'DESC')->get();
        $data = [];
        foreach ($artikels as $artikel) {
            $users = User::where('id', $artikel['id_user']) -> get();
            foreach ($users as $user) {
                $artikel['writer'] = $user['name'];
            }
            $data[] = $artikel;
        }
        if($id != -1)
        {
            $users = User::where('id', $id) -> get();
            foreach ($users as $user) {
                $name = $user['name'];
                $isadmin = $user['is_admin'];
            }
        }

        if($id == -1)
        {
            return view('listArtikel', compact('nameArtikel','data'));
        }else {
            return view('listArtikel', compact('id','nameArtikel','data','name', 'isadmin'));
        }
    }

    public function detailArtikel($artikel_id, $id=-1) {
        $artikels = Artikel::where('id', $artikel_id)->get();
        $SUMCOMMENT = 0;
        $data = [];
        foreach ($artikels as $artikel) {
            $data = $artikel;
        }
        if($id != -1)
        {
            $users = User::where('id', $id) -> get();
            foreach ($users as $user) {
                $name = $user['name'];
                $isadmin = $user['is_admin'];
            }
        }

        $claps = Clap::where('artikel_id', $artikel_id) -> get();
        $sumClaps = 0;
        foreach ($claps as $clap) {
            $sumClaps += 1;
        }

        $comments = Comment::latest()->where('artikel_id', $artikel_id) -> get();
        $dataComment = [];
        foreach ($comments as $comment) {
            $SUMCOMMENT += 1;
            $likes = Like::where('comment_id', $comment['id'])->where('type_comment','comment') -> get();
            $dislikes = Dislike::where('comment_id', $comment['id'])->where('type_comment','comment') -> get();
            $subcomments = Subcomment::where('comment_id', $comment['id']) -> get();
            $user = User::where('id', $comment['user_id']) -> get();
            foreach ($user as $us)
                $comment['username'] = $us['name'];
            $sumLikes = 0;
            $sumDislikes = 0;
            foreach ($likes as $Like)
                $sumLikes += 1;
            foreach ($dislikes as $Dislike)
                $sumDislikes += 1;

            $checkuserlike = Like::where('comment_id', $comment['id'])->where('user_id', $id)->where('type_comment','comment') -> get();

            $comment['liked'] = 0;
            $comment['like_id'] = -1;
            foreach ($checkuserlike as $check)
            {
                $comment['liked'] = 1;
                $comment['like_id'] = $check['id'];
            }

            $checkuserdislike = Dislike::where('comment_id', $comment['id'])->where('user_id', $id)->where('type_comment','comment') -> get();
            $comment['disliked'] = 0;
            $comment['dislike_id'] = -1;
            foreach ($checkuserdislike as $check)
            {
                $comment['disliked'] = 1;
                $comment['dislike_id'] = $check['id'];
            }

            //#Sub Comment*/
            $dataSubComment = [];
            foreach ($subcomments as $subcomment) {
                $SUMCOMMENT += 1;
                $user = User::where('id', $subcomment['user_id']) -> get();
                $sublikes = Like::where('comment_id', $subcomment['id'])->where('type_comment','subcomment') -> get();
                $subdislikes = Dislike::where('comment_id', $subcomment['id'])->where('type_comment','subcomment') -> get();
                foreach ($user as $us)
                    $subcomment['username'] = $us['name'];
                $sumLikesSub = 0;
                $sumDislikesSub = 0;
                foreach ($sublikes as $Like)
                    $sumLikesSub += 1;
                foreach ($subdislikes as $Dislike)
                    $sumDislikesSub += 1;
                $subcomment['sumlikes']  = $sumLikesSub;
                $subcomment['sumdislikes']  = $sumDislikesSub;

                $checkuserlike = Like::where('comment_id', $subcomment['id'])->where('user_id', $id)->where('type_comment','subcomment') -> get();

                $subcomment['liked'] = 0;
                $subcomment['like_id'] = -1;
                foreach ($checkuserlike as $check)
                {
                    $subcomment['liked'] = 1;
                    $subcomment['like_id'] = $check['id'];
                }

                $checkuserdislike = Dislike::where('comment_id', $subcomment['id'])->where('user_id', $id)->where('type_comment','subcomment') -> get();
                $subcomment['disliked'] = 0;
                $subcomment['dislike_id'] = -1;
                foreach ($checkuserdislike as $check)
                {
                    $subcomment['disliked'] = 1;
                    $subcomment['dislike_id'] = $check['id'];
                }

                $dataSubComment[] = $subcomment;
            }
            $comment['subComment'] = $dataSubComment;
            $comment['sumlikes'] = $sumLikes;
            $comment['sumdislikes'] = $sumDislikes;
            $dataComment[] = $comment;
        }

        if($id == -1)
        {
            return view('artikel', compact('data', 'sumClaps', 'dataComment', 'SUMCOMMENT'));
        }else {
            return view('artikel', compact('id','data','name', 'isadmin', 'sumClaps', 'dataComment', 'SUMCOMMENT'));
        }
    }

    public function addClaps($id_artikel, $id_user) {
        $claps = new Clap();
        $claps->artikel_id = $id_artikel;
        $claps->user_id = $id_user;

        $claps->save();
        return redirect('artikell/'.$id_artikel.'/'.$id_user);
    }

    public function removeClaps($id_artikel, $id_user) {
        $claps = Clap::where("artikel_id", $id_artikel)->where('user_id', $id_user);
        if($claps)
            $claps->delete();
        return redirect('artikell/'.$id_artikel.'/'.$id_user);
    }

    public function addComment(Request $request) {
        $comments = new Comment();
        $comments->comment_text = $request['text-komen'];
        $comments->user_id = $request['user_id'];
        $comments->artikel_id = $request['artikel_id'];

        $comments->save();
        return redirect('artikell/'.$request['artikel_id'].'/'.$request['user_id']);
    }

    public function addLike($id_artikel, $id_user, $comment_id, $dislike_id, $type) {
        $likes = new Like();
        $likes->comment_id = $comment_id;
        $likes->user_id = $id_user;
        $likes->type_comment = $type;
        $likes->artikel_id = $id_artikel;
        $likes->save();

        $dislikes = Dislike::find($dislike_id);
        if($dislikes)
            $dislikes->delete();

        return redirect('artikell/'.$id_artikel.'/'.$id_user);
    }

    public function removeLike($id_artikel, $id_user, $like_id) {
        $likes = Like::find($like_id);
        $likes->delete();
        return redirect('artikell/'.$id_artikel.'/'.$id_user);
    }

    public function addDislike($id_artikel, $id_user, $comment_id, $like_id, $type) {
        $dislikes = new Dislike();
        $dislikes->comment_id = $comment_id;
        $dislikes->user_id = $id_user;
        $dislikes->type_comment = $type;
        $dislikes->artikel_id = $id_artikel;
        $dislikes->save();

        $likes = Like::find($like_id);
        if($likes)
            $likes->delete();

        return redirect('artikell/'.$id_artikel.'/'.$id_user);
    }

    public function removeDislike($id_artikel, $id_user, $dislike_id) {
        $dislikes = Dislike::find($dislike_id);
        $dislikes->delete();
        return redirect('artikell/'.$id_artikel.'/'.$id_user);
    }

    public function addSubComment(Request $request) {
        $subcomments = new Subcomment();
        $subcomments->comment_text = $request['sub_comment_text'];
        $subcomments->comment_id = $request['comment_id'];
        $subcomments->user_id = $request['user_id'];
        $subcomments->artikel_id = $request['artikel_id'];

        $subcomments->save();
        return redirect('artikell/'.$request['artikel_id'].'/'.$request['user_id']);
    }

}
